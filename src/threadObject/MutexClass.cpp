//
// MutexClass.cpp: implementation file
//
// Copyright (C) Walter E. Capers.  All rights reserved
//
// This source is free to use as you like.  If you make
// any changes please keep me in the loop.  Email them to
// walt.capers@comcast.net.
//
// PURPOSE:
//
//  To implement mutexes as a C++ object
//
// REVISIONS
// =======================================================
// Date: 10.25.07        
// Name: Walter E. Capers
// Description: File creation
//
// Date:
// Name:
// Description:
//
//

#include "Thread.h"

CMutexClass::CMutexClass (void)
  : m_bCreated (TRUE)
{
   pthread_mutexattr_t mattr;
   pthread_mutexattr_init (&mattr);
   pthread_mutex_init (&m_mutex, &mattr);
}

CMutexClass::~CMutexClass (void)
{
  pthread_mutex_lock (&m_mutex);
  pthread_mutex_unlock (&m_mutex);
  pthread_mutex_destroy (&m_mutex);
}

void
CMutexClass::Lock ()
{
  ThreadId_t id = CThread::ThreadId ();
  if (CThread::ThreadIdsEqual (&m_owner, &id))
    return; /* The mutex is already locked by this thread. */

  pthread_mutex_lock (&m_mutex);

  m_owner = CThread::ThreadId ();
}

void 
CMutexClass::Unlock()
{
  ThreadId_t id = CThread::ThreadId ();
  if (!CThread::ThreadIdsEqual (&id, &m_owner) )
    return; /* On the thread that has locked the mutex can release 
               the mutex. */

  memset (&m_owner, 0, sizeof (ThreadId_t));
  pthread_mutex_unlock (&m_mutex);
}
