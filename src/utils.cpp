/* Copyright (C) 2009-2013  The Dawn Team

   This file is a part of the dawn-rpg project.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "utils.hpp"
#include "configuration.hpp"
#include "GLee/GLee.h"
#include <sstream>
#include <cstring>
#include <memory>

namespace DawnInterface
{
  void addTextToLogWindow (GLfloat color[], const char *text, ...);
}

extern int world_x, world_y;

bool utils::file_exists (const std::string &file)
{
  std::ifstream temp(file.c_str());

  if (!temp)
    return false;

  return true;
}
