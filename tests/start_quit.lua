/* Copyright (C) 2009-2012 Dawn - 2D roleplaying game

   This file is a part of the dawn-rpg project.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

local state = TestInterface.getDawnState();

-- Dawn's states:
-- 0 - NoState
-- 1 - MainMenu
-- 2 - ChooseClass
-- 3 - LoadingScreen
-- 4 - OptionsMenu
-- 5 - InGame

-- wait until we get to the MainMenu
while true do
    state = TestInterface.getDawnState();
    if state == 1 then
        break
    end
end

-- we're in the Main Menu, now click on choose class
TestInterface.clickOnNewGame();

-- wait until we get to the choose class screen
while true do
    state = TestInterface.getDawnState();
    if state == 2 then
        break
    end
end

-- we're in the choose class screen, pick a class
TestInterface.chooseClass( CharacterClass.Liche );

-- wait until we get inside the game
while true do
    state = TestInterface.getDawnState();
    if state == 5 then
        break
    end
end

-- we're in the game, exit Dawn
TestInterface.quitDawn(0);