# Copyright (C) 2009-2012 Dawn - 2D roleplaying game

#   This file is a part of the dawn-rpg project.

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash

if [ ! -e `dirname $0`/dawn-rpg ]
then
	echo "Could not find file `dirname $0`/dawn-rpg. Please compile dawn
              first using 'cd `dirname $0` && ./configure && make'."
	exit 1
fi

pushd `dirname $0`
./dawn-rpg "$@" | ./errorparser.sh
popd > /dev/null
