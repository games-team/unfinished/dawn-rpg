-- Copyright (C) 2009-2013  The Dawn Team
--
--   This file is a part of the dawn-rpg project.
--
--   This program is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- init quest namespace so different quests / zones don't conflict in names
if( quest_hungryman == nil )
then
  quest_hungryman = {}
end

function quest_hungryman.init()
  quest_hungryman.henry = DawnInterface.addMobSpawnPoint( "Human", 500, 600, 1, 0 );
  quest_hungryman.henry:setAttitude( Attitude.FRIENDLY );
  quest_hungryman.henry:setName( "Henry" );
  quest_hungryman.henryInteraction = DawnInterface.addCharacterInteractionPoint( quest_hungryman.henry );
  quest_hungryman.henryInteraction:setInteractionType( InteractionType.Quest );
  quest_hungryman.henryInteraction:setInteractionCode( "quest_hungryman.speakWithMan()" );
end

function quest_hungryman.speakWithMan()
  if( not quest_hungryman.completed ) then
    if( quest_hungryman.added == nil ) then
      quest_hungryman.added = true;
      quest_hungryman.completed = false;
      quest_hungryman.gotReward = false;
      quest_hungryman.quest = DawnInterface.addQuest( "Hungry Man", "Henry is very hungry! Bring him five pieces of wolf meat." );
      quest_hungryman.quest:setExperienceReward( 45 );
      quest_hungryman.quest:setCoinReward( 200 );
      quest_hungryman.quest:addRequiredItemForCompletion (itemDatabase["wolfmeat"], 5);
      quest_hungryman.showManText( 1 );
    elseif (quest_hungryman.added == true) then
      if (quest_hungryman.quest:finishQuest() == true) then
        quest_hungryman.showManText( 3 );
        quest_hungryman.completed = true;
        quest_hungryman.quest = nil;
      else
        quest_hungryman.showManText (2);
      end
    end
  else
    quest_hungryman.showManText( 4 );
  end  
end

function quest_hungryman.showManText( part )
  if( part == 1 ) then
    local textWindow = DawnInterface.createTextWindow();
    textWindow:setPosition( PositionType.CENTER, 512, 382 );
    textWindow:setText( "I'm hurt and can't hunt. I need food. Could you bring me some wolf meat?" );
    textWindow:setAutocloseTime( 0 );
  end
  if( part == 2 ) then
    local textWindow = DawnInterface.createTextWindow();
    textWindow:setPosition( PositionType.CENTER, 512, 382 );
    textWindow:setText( "I'm starving!" );
    textWindow:setAutocloseTime( 0 );
  end
  if( part == 3 ) then
    local textWindow = DawnInterface.createTextWindow();
    textWindow:setPosition( PositionType.CENTER, 512, 382 );
    textWindow:setText( "Thank you stranger!" );
    textWindow:setAutocloseTime( 0 );
  end
  if( part == 4 ) then
    local textWindow = DawnInterface.createTextWindow();
    textWindow:setPosition( PositionType.CENTER, 512, 382 );
    textWindow:setText( "You saved my life!" );
    textWindow:setAutocloseTime( 0 );
  end
end

-- init quest if this has not been done yet
if( quest_hungryman.inited == nil ) then
  quest_hungryman.inited = true;
  quest_hungryman.init();
end
