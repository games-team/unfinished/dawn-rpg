-- Copyright (C) 2009-2013  The Dawn Team

--   This file is a part of the dawn-rpg project.

--   This program is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.

--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.

--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.

--note: this is just for debug the items need to be copied not accessed directly else the same reference is used!!!

local thePlayer = DawnInterface.getPlayer();

-- items to give to our warriors
if (thePlayer:getClass () == CharacterClass.Warrior) then
  DawnInterface.giveItemToPlayer (itemDatabase["axeofanguish"]);
  DawnInterface.giveItemToPlayer (itemDatabase["swordofkhazom"]);
  DawnInterface.giveItemToPlayer (itemDatabase["tornleatherbelt"]);
  DawnInterface.giveItemToPlayer (itemDatabase["platehelmet"]);
end

-- items to give to our rangers
if ( thePlayer:getClass() == CharacterClass.Ranger ) then
  DawnInterface.giveItemToPlayer(itemDatabase["weakenedbow"] );
  DawnInterface.giveItemToPlayer( itemDatabase["compoundbow"] );
  DawnInterface.giveItemToPlayer( itemDatabase["tornleatherbelt"] );
end

-- items to give to our liches
if ( thePlayer:getClass() == CharacterClass.Liche ) then
  DawnInterface.giveItemToPlayer( itemDatabase["bookofmagicmissilerank2"] );
  DawnInterface.giveItemToPlayer( itemDatabase["moldytome"] );
  DawnInterface.giveItemToPlayer( itemDatabase["daggerofflowingthought"] );
  DawnInterface.giveItemToPlayer( itemDatabase["elvishbelt"] );
end

DawnInterface.giveItemToPlayer( itemDatabase["eyeoflicor"] );
DawnInterface.giveItemToPlayer( itemDatabase["gnollshield"] );
DawnInterface.giveItemToPlayer( itemDatabase["gutteraxe"] );
DawnInterface.giveItemToPlayer( itemDatabase["snakeloop"] );
DawnInterface.giveItemToPlayer( itemDatabase["shadering"] );
DawnInterface.giveItemToPlayer( itemDatabase["scrolloftheboar"] );
DawnInterface.giveItemToPlayer( itemDatabase["scrolloftheboar"] );
DawnInterface.giveItemToPlayer( itemDatabase["scrolloftheboar"] );
DawnInterface.giveItemToPlayer( itemDatabase["smallhealingpotion"] );
DawnInterface.giveItemToPlayer( itemDatabase["coppernecklace"] );
DawnInterface.giveItemToPlayer( itemDatabase["fungalboots"] );
DawnInterface.giveItemToPlayer( itemDatabase["ajuicyapple"] );
DawnInterface.giveItemToPlayer( itemDatabase["ajuicyapple"] );
DawnInterface.giveItemToPlayer( itemDatabase["awaterpouch"] );
DawnInterface.giveItemToPlayer( itemDatabase["awaterpouch"] );
DawnInterface.giveItemToPlayer( itemDatabase["awaterpouch"] );
DawnInterface.giveItemToPlayer( itemDatabase["awaterpouch"] );
DawnInterface.giveItemToPlayer( itemDatabase["awaterpouch"] );
DawnInterface.giveItemToPlayer( itemDatabase["windstone"] );
--note: this is just for debug the items need to be copied not accessed directly else the same reference is used!!!

dofile ("data/quests_wood.lua");
