# Copyright (C) 2009-2013  The Dawn Team
#
#   This file is a part of the dawn-rpg project.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
# This script will help us to keep track of all art in Dawn.
# Running this script checks for any new files that are not 
# listed in AssetsLicenceInventory.txt
# Currently searches for *.tga or *.ogg

FILES=$(find data/ -name "*.tga" -o -name "*.ogg")

# setting the internal field seperator to newline
IFS=$'\n'

# our filecounter, just to display dots that we are working.
filecounter=0

for filename in ${FILES}
do
	# just a visual aid to show us that the program is searching. adds a dot every 200 files.
	let filecounter=filecounter+1
	if [ $filecounter -gt 200 ]; then
		echo -n "."
		let filecounter=0
	fi
	
	# we see if the file is listed in our AssetsLicenceInventory.txt, if not we add it and warn the user about it.
	findfile=`cat AssetsLicenceInventory.txt | grep "$filename"`
	if [ -z "$findfile" ]; then 
		echo -e "\n$filename isn't listed in AssetsLicenceInventory.txt."
		echo "Make sure you document the licence and the source of this file"
		echo "$filename" >> AssetsLicenceInventory.txt
	fi
done
